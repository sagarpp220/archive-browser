export const K_AMPLITUDE_EVENT_PAGE_VIEW = "page_view";

export const K_AMPLITUDE_EVENT_VIDEO_PLAY = "video_play";
export const K_AMPLITUDE_EVENT_VIDEO_PAUSE = "video_pause";
export const K_AMPLITUDE_EVENT_VIDEO_SEEK = "video_seek";
export const K_AMPLITUDE_EVENT_VIDEO_MUTE_TOGGLE = "video_mute_toggle";
export const K_AMPLITUDE_EVENT_VIDEO_VOLUME_ADJUST = "video_volume_adjust";
export const K_AMPLITUDE_EVENT_VIDEO_CAPTIONS_CYCLE = "video_captions_cycle";
export const K_AMPLITUDE_EVENT_VIDEO_FULLSCREEN_TOGGLE =
  "video_fullscreen_toggle";
export const K_AMPLITUDE_EVENT_VIDEO_READY_STATE = "video_ready_state";
export const K_AMPLITUDE_EVENT_VIDEO_PLAYBACK_ERROR = "video_playback_error";

export const K_AMPLITUDE_EVENT_VIDEO_BUTTON_MORE_DOWNLOADS =
  "video_button_more_downloads";
export const K_AMPLITUDE_EVENT_VIDEO_DOWNLOAD = "video_download";

export const K_AMPLITUDE_EVENT_SPEED_TEST = "speed_test";
export const K_AMPLITUDE_EVENT_ARCHIVAL_REQUEST = "archival_request";
