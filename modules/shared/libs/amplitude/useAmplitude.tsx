import React from "react";
import AmplitudeContext from "./AmplitudeContext";

export const useAmplitude = () => React.useContext(AmplitudeContext);
